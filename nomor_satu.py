#convert data excel to xls

#import library yang diperlukan
import csv
import openpyxl
#membuka file excel
wb = openpyxl.load_workbook('Matrix.xlsx')

#A. Sheet Vulnerability
#mengaktifkan sheet vulnerability
sheet_aktif = wb.get_sheet_by_name('Vulnerability')

#mencari nilai baris tertinggi dari sheet vulnerability
baris = sheet_aktif.get_highest_row()


#menulis ke file vulnerability.csv
with open('vulnerability.csv', 'w', newline='') as csvfile:
    file_csv = csv.writer(csvfile, delimiter=',',quoting=csv.QUOTE_MINIMAL)
    #untuk baris 1 sampai baris tertinggi+1
    for i in range(1, baris+1, 1):
    	
    	file_csv.writerow([sheet_aktif.cell(row=i, column=1).value , sheet_aktif.cell(row=i, column=2).value , sheet_aktif.cell(row=i, column=3).value,sheet_aktif.cell(row=i, column=4).value  ])


print("Konversi Sheet Vulnerability telah berhasil !")   


#B. Sheet MMI
#mengaktifkan sheet MMI
sheet_aktif = wb.get_sheet_by_name('MMI')


data_mmi = []

#x ada 20 karena ada 20 baris
for x in range(0,20,1):
        data_mmi.append([])
        #y ada 20 karena ada 20 kolom
        for y in range(0,20,1):
            data_mmi[x].append(format(sheet_aktif.cell(row=x+1, column=y+1).value,'.2f'))
        

#menulis ke file mmi.csv
with open('mmi.csv', 'w', newline='') as csvfile:
    file_csv = csv.writer(csvfile, delimiter=',',quoting=csv.QUOTE_MINIMAL)
    #untuk baris 1 sampai baris tertinggi+1
    for i in range(1, 21, 1):
    	file_csv.writerow(data_mmi[i-1])


print("Konversi Sheet MMI telah berhasil !")   
	

#C. Sheet BUILDING TYPE
#mengaktifkan sheet Building Type
sheet_aktif = wb.get_sheet_by_name('Building_Type')


data_building = []

#x ada 20 karena ada 20 baris
for x in range(0,20,1):
        data_building.append([])
        #y ada 20 karena ada 20 kolom
        for y in range(0,20,1):
            data_building[x].append(sheet_aktif.cell(row=x+1, column=y+1).value)
        

#menulis ke file mmi.csv
with open('building_type.csv', 'w', newline='') as csvfile:
    file_csv = csv.writer(csvfile, delimiter=',',quoting=csv.QUOTE_MINIMAL)
    #untuk baris 1 sampai baris tertinggi+1
    for i in range(1, 21, 1):
    	file_csv.writerow(data_building[i-1])


print("Konversi Sheet Building Type telah berhasil !")   

#D. Sheet BUILDING VALUE
#mengaktifkan sheet Building Value
sheet_aktif = wb.get_sheet_by_name('Building_Value_dalam_juta_Rp')


building_value = []

#x ada 20 karena ada 20 baris
for x in range(0,20,1):
        building_value.append([])
        #y ada 20 karena ada 20 kolom
        for y in range(0,20,1):
            building_value[x].append(sheet_aktif.cell(row=x+1, column=y+1).value)
        

#menulis ke file mmi.csv
with open('building_value.csv', 'w', newline='') as csvfile:
    file_csv = csv.writer(csvfile, delimiter=',',quoting=csv.QUOTE_MINIMAL)
    #untuk baris 1 sampai baris tertinggi+1
    for i in range(1, 21, 1):
    	file_csv.writerow(building_value[i-1])


print("Konversi Sheet Building Value telah berhasil !")   
	
		