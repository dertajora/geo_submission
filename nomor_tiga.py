#import library
import matplotlib.pyplot as plt
import numpy as np
import csv
from prettytable import PrettyTable

#fungsi untuk membaca file csv
def read_csv(file_name):
	open_file = open(file_name)
	read_file = csv.reader(open_file)
	data = list(read_file)
	return data

#fungsi untuk mencari nilai damage berdasarkan nilai mmi
def find_damage(x1,x2,x3,x4,x5,x6,x7,mmi):
	if mmi<5:
		damage_value = x1
	elif mmi<6:
		damage_value = x2
	elif mmi<7:
		damage_value = x3
	elif mmi<8:
		damage_value = x4
	elif mmi<9:
		damage_value = x5
	elif mmi<10:
		damage_value = x6
	elif mmi>=10:
		damage_value = x7

	return	damage_value

#fungsi untuk menghitung total bangunan per tipe dan total damage per tipe
def hitung_damage_per_tipe(tipe,damage):
	jumlah_tipe[tipe-1]+=1
	percentage_damage_per_tipe[tipe-1] = percentage_damage_per_tipe[tipe-1]+damage

def hitung_mmi_per_tipe(tipe,mmi):
	data_mmi_per_tipe[tipe-1] = data_mmi_per_tipe[tipe-1]+mmi

#memanggil fungsi untuk membaca masing-masing csv
data_mmi = read_csv('mmi.csv')
building_type = read_csv('building_type.csv')
building_value = read_csv('building_value.csv')
building_vulnerability = read_csv('vulnerability.csv')

#inisiasi array data mmi per tipe, percentage damage per tipe, dan jumlah bangunan per tipe
data_mmi_per_tipe = [0 for x in range(10) ]
jumlah_tipe = [0 for x in range(10) ]
percentage_damage_per_tipe = [0 for x in range(10) ]

#convert array 2D string into float and int
for x in range(20):
	for y in range(20):
		data_mmi[x][y] = float(data_mmi[x][y])
		building_type[x][y] = int(building_type[x][y])


#MENGHITUNG DATA MMI PER TIPE
for x in range(20):
	for y in range(20):
		hitung_mmi_per_tipe(building_type[x][y],data_mmi[x][y])		

	 
#MENGHITUNG DATA PERCENTAGE DAMAGE PER TIPE

#inisiasi array untuk menyimpan data damage untuk tiap bangunan
damage = [[0 for x in range(20)] for x in range(20)] 
#membuat matriks damage
for x in range(20):
	for y in range(20):
		#deklarasi nilai mmi untuk data[x][y]
		mmi = data_mmi[x][y]
		
		#mengetahui percentage damage untuk tiap bangunan
		if(building_type[x][y]==1):
			#penyimpanan nilai damage value
			damage[x][y]=find_damage(0,20,28,33,35,71,95,mmi)
		elif(building_type[x][y]==2):
			#penyimpanan nilai damage value
			damage[x][y]=find_damage(0,23,29,36,43,61,75,mmi)
		elif(building_type[x][y]==3):
			#penyimpanan nilai damage value
			damage[x][y]=find_damage(0,0,28,36,36,62,81,mmi)	
		elif(building_type[x][y]==4):
			#penyimpanan nilai damage value
			damage[x][y]=find_damage(0,22,30,37,42,65,83,mmi)
		elif(building_type[x][y]==5):
			#penyimpanan nilai damage value
			damage[x][y]=find_damage(0,13,34,44,50,73,79,mmi)
		elif(building_type[x][y]==6):
			#penyimpanan nilai damage value
			damage[x][y]=find_damage(0,17,25,31,37,59,95,mmi)
		elif(building_type[x][y]==7):
			#penyimpanan nilai damage value
			damage[x][y]=find_damage(0,24,26,36,50,52,86,mmi)
		elif(building_type[x][y]==8):
			#penyimpanan nilai damage value
			damage[x][y]=find_damage(0,8,35,38,41,66,93,mmi)
		elif(building_type[x][y]==9):
			#penyimpanan nilai damage value
			damage[x][y]=find_damage(0,7,35,44,49,65,93,mmi)
		elif(building_type[x][y]==10):
			#penyimpanan nilai damage value
			damage[x][y]=find_damage(0,20,35,40,45,71,95,mmi)
			
		#menambahkan ke data damage per tipe
		hitung_damage_per_tipe(building_type[x][y],damage[x][y])

#memformat decimal places data mmi per tipe
for x in range(10):
	percentage_damage_per_tipe[x] = float("{0:.3f}".format(percentage_damage_per_tipe[x])) 
	data_mmi_per_tipe[x] = float("{0:.3f}".format(data_mmi_per_tipe[x]))  


#mendefinisikan array yang berisi tipe building
type_building = [x for x in range(1,11) ]

#menghitung rata-rata percentage damage dan mmi untuk masing-masing tipe
rerata_mmi = []
rerata_damage = []
for x in range(0,10):
	rerata_damage.append(percentage_damage_per_tipe[x]/jumlah_tipe[x])

for x in range(0,10):
	rerata_mmi.append(data_mmi_per_tipe[x]/jumlah_tipe[x])


#mengatur decimal places
for x in range(10):
	rerata_mmi[x] = float("{0:.2f}".format(rerata_mmi[x]))  

for x in range(10):
	rerata_damage[x] = float("{0:.2f}".format(rerata_damage[x]))  


#menulis pesan konfirmasi
print("")
print("Penghitungan data percentage of damage dan MMI telah berhasil !")
print("Pembuatan grafik telah berhasil !")
print("")

print("")
print("-------------------------------------------------------DATA HASIL KOMPUTASI-------------------------------------------------------")
print("")
t = PrettyTable(['Building Type','Number of Buildings','Total of Percentage Damage', 'Total of MMI','Average of Percentage Damage','Average of MMI'])
for x in range(10):
	t.add_row([type_building[x], jumlah_tipe[x],percentage_damage_per_tipe[x],data_mmi_per_tipe[x],rerata_damage[x],rerata_mmi[x]])
print(t)


#inisiasi list data summary
data_summary = [[0,0,0,0] for x in range(10) ]
#update element of list dari data summary
for x in range(0,10):
	data_summary[x][0] = type_building[x]
	data_summary[x][1] = jumlah_tipe[x]
	data_summary[x][2] = data_mmi_per_tipe[x]
	data_summary[x][3] = percentage_damage_per_tipe[x]
	


#mengurutkan list berdasarkan nilai array
data_summary.sort(key=lambda x: int(x[2]))

#nilai untuk membuat grafik mmi vs damage
damage_graph = [item[3] for item in data_summary]
mmi_graph = [item[2] for item in data_summary]
type_building_graph = [item[0] for item in data_summary]

#MEMBUAT GRAFIK

#menampilkan grafik total percentage damage and MMI
plt.figure('Total of Percentage Damage and MMI for each Building Type')
#menyesuaikan posisi plot di gambar
plt.subplots_adjust(top=0.8)
#konfigurasi style dan content garis pertama
plt.plot(type_building, percentage_damage_per_tipe,marker='*', label='Percentage of Damage')
#konfigurasi style dan content  garis kedua
plt.plot(type_building, data_mmi_per_tipe, marker='o', linestyle='--', color='r', label='MMI')
#mengatur label X
plt.xlabel('Building Type')
#mengatur label Y
plt.ylabel('Value')
#mengatur judul grafik
plt.title('Total of Percentage Damage and MMI for each Building Type')
#menampilkan dan mengatur legend
plt.legend(loc=(0.505,1.1))

#menampilkan grafik average percentage damage and MMI
plt.figure('Average of Percentage Damage and MMI for each Building Type')
#menyesuaikan posisi plot di gambar
plt.subplots_adjust(top=0.8)
#konfigurasi style dan content garis pertama
plt.plot(type_building, rerata_damage,marker='*', label='Percentage of Damage')
#konfigurasi style dan content  garis kedua
plt.plot(type_building, rerata_mmi, marker='o', linestyle='--', color='r', label='MMI')
#mengatur label X
plt.xlabel('Building Type')
#mengatur label Y
plt.ylabel('Value')
#mengatur judul grafik
plt.title('Average of Percentage Damage and MMI for each Building Type')
#mengatur dan mengatur legend
plt.legend(loc=(0.505,1.1))


fig, ax = plt.subplots()
#menyesuaikan posisi plot di gambar
plt.subplots_adjust(top=0.85)
#set plot line
ax.plot(mmi_graph,damage_graph,marker='o', label='Percentage Damage vs MMI for each Building Type')
#set annotation in line marker
for i, txt in enumerate(type_building_graph):
    ax.annotate(txt, (mmi_graph[i],damage_graph[i]))
#mengatur label X
plt.xlabel('Total of MMI')
#mengatur label Y
plt.ylabel('Total of Percentage Damage')
#menampilkan legend
plt.legend(loc=(0.028,1.1)) 

#menmanggil grafik
plt.show()

		
