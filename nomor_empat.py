#import library
import matplotlib.pyplot as plt
import numpy as np
import csv
from prettytable import PrettyTable

#fungsi untuk membaca file csv
def read_csv(file_name):
	open_file = open(file_name)
	read_file = csv.reader(open_file)
	data = list(read_file)
	return data

#fungsi untuk mencari nilai damage berdasarkan nilai mmi
def find_damage(x1,x2,x3,x4,x5,x6,x7,mmi):
	if mmi<5:
		damage_value = x1
	elif mmi<6:
		damage_value = x2
	elif mmi<7:
		damage_value = x3
	elif mmi<8:
		damage_value = x4
	elif mmi<9:
		damage_value = x5
	elif mmi<10:
		damage_value = x6
	elif mmi>=10:
		damage_value = x7

	return	damage_value

#fungsi untuk menghitung total bangunan per tipe dan total damage per tipe
def hitung_damage_per_tipe(tipe,damage):
	jumlah_tipe[tipe-1]+=1
	data_damage_per_tipe[tipe-1] = data_damage_per_tipe[tipe-1]+damage

#fungsi untuk menghitung total loss per tipe bangunan
def hitung_loss_per_tipe(tipe,loss_value):
	loss_value_per_tipe[tipe-1] = loss_value_per_tipe[tipe-1]+loss_value

#fungsi untuk menghitung total MMI per tipe bangunan
def hitung_mmi_per_tipe(tipe,mmi):
	data_mmi_per_tipe[tipe-1] = data_mmi_per_tipe[tipe-1]+mmi	

#mendefinisikan array untuk menyimpan data mmi per tipe
loss_value_per_tipe = [0 for x in range(10) ]
data_damage_per_tipe = [0 for x in range(10) ]
data_mmi_per_tipe = [0 for x in range(10) ]
jumlah_tipe = [0 for x in range(10) ]
type_building = [x for x in range(1,11) ]

#memanggil fungsi untuk membaca masing-masing csv
data_mmi = read_csv('mmi.csv')
building_type = read_csv('building_type.csv')
building_vulnerability = read_csv('vulnerability.csv')
building_value = read_csv('building_value.csv')

#convert array 2D string into float and int
for x in range(20):
	for y in range(20):
		data_mmi[x][y] = float(data_mmi[x][y])

for x in range(20):
	for y in range(20):
		building_type[x][y] = int(building_type[x][y])

for x in range(20):
	for y in range(20):
		building_value[x][y] = int(building_value[x][y])

#inisiasi matriks damage dan loss
damage = [[0 for x in range(20)] for x in range(20)] 
loss = [[0 for x in range(20)] for x in range(20)] 


#A. MATRIKS DAMAGE
#membuat matriks damage
for x in range(20):
	for y in range(20):
		#deklarasi nilai mmi untuk data[x][y]
		mmi = data_mmi[x][y]
		
		#mengetahui percentage damage untuk tiap bangunan
		if(building_type[x][y]==1):
			#penyimpanan nilai damage value
			damage[x][y]=find_damage(0,20,28,33,35,71,95,mmi)
		elif(building_type[x][y]==2):
			#penyimpanan nilai damage value
			damage[x][y]=find_damage(0,23,29,36,43,61,75,mmi)
		elif(building_type[x][y]==3):
			#penyimpanan nilai damage value
			damage[x][y]=find_damage(0,0,28,36,36,62,81,mmi)	
		elif(building_type[x][y]==4):
			#penyimpanan nilai damage value
			damage[x][y]=find_damage(0,22,30,37,42,65,83,mmi)
		elif(building_type[x][y]==5):
			#penyimpanan nilai damage value
			damage[x][y]=find_damage(0,13,34,44,50,73,79,mmi)
		elif(building_type[x][y]==6):
			#penyimpanan nilai damage value
			damage[x][y]=find_damage(0,17,25,31,37,59,95,mmi)
		elif(building_type[x][y]==7):
			#penyimpanan nilai damage value
			damage[x][y]=find_damage(0,24,26,36,50,52,86,mmi)
		elif(building_type[x][y]==8):
			#penyimpanan nilai damage value
			damage[x][y]=find_damage(0,8,35,38,41,66,93,mmi)
		elif(building_type[x][y]==9):
			#penyimpanan nilai damage value
			damage[x][y]=find_damage(0,7,35,44,49,65,93,mmi)
		elif(building_type[x][y]==10):
			#penyimpanan nilai damage value
			damage[x][y]=find_damage(0,20,35,40,45,71,95,mmi)
			
		#menambahkan ke data damage per tipe
		hitung_damage_per_tipe(building_type[x][y],damage[x][y])

#B. MATRIKS LOSS
#membuat matriks loss
for x in range(20):
	for y in range(20):
		#menghitung nilai kerugian untuk tiap bangunan 
		loss[x][y]=damage[x][y]*building_value[x][y]/100
		#menambbahkan ke loss per tipe bangunan
		hitung_loss_per_tipe(building_type[x][y],loss[x][y])


#konversi matriks damage ke file csv
with open('damage.csv', 'w', newline='') as csvfile:
    file_csv = csv.writer(csvfile, delimiter=',',quoting=csv.QUOTE_MINIMAL)
    #untuk baris 1 sampai baris tertinggi+1
    for i in range(20):
 	   	file_csv.writerow(damage[i])

#konversi matriks damage ke file csv
with open('loss.csv', 'w', newline='') as csvfile:
    file_csv = csv.writer(csvfile, delimiter=',',quoting=csv.QUOTE_MINIMAL)
    #untuk baris 1 sampai baris tertinggi+1
    for i in range(20):
 	   	file_csv.writerow(loss[i])

#C. MENAMPILKAN GRAFIK PLOT 2D UNTUK MATRIKS DAMAGE DAN LOSS

#menampilkan grafik building value
fig, ax = plt.subplots(1)
p = ax.pcolormesh(damage)
fig.colorbar(p)
ax.set_title("Persentage of Damage for Each Building")

#menampilkan grafik building type
fig, ax = plt.subplots(1)
p = ax.pcolormesh(loss)
fig.colorbar(p)
ax.set_title("Loss Value for Each Building")


print("-------------")
print("Pembuatan Matriks Damage Berhasil !")
print("Konversi Matriks Damage ke File CSV Berhasil !")
print("Pembuatan Matriks Loss Berhasil !")
print("Konversi Matriks Loss ke File CSV Berhasil !")
print("Pembuatan Grafik Plot 2D Berhasil !")



#D. MENAMPILKAN GRAFIK ANTARA LOSS VALUE DAN MMI UNTUK SETIAP BUILDING TYPE

#menghitung data mmi per tipe
for x in range(20):
	for y in range(20):
		hitung_mmi_per_tipe(building_type[x][y],data_mmi[x][y])


#menghitung rata-rata loss value dan mmi untuk masing-masing tipe
rerata_loss = []
rerata_mmi = []
for x in range(0,10):
	rerata_loss.append(loss_value_per_tipe[x]/jumlah_tipe[x])

for x in range(0,10):
	rerata_mmi.append(data_mmi_per_tipe[x]/jumlah_tipe[x])


#mengatur decimal places
for x in range(10):
	rerata_mmi[x] = float("{0:.2f}".format(rerata_mmi[x]))  

for x in range(10):
	rerata_loss[x] = float("{0:.2f}".format(rerata_loss[x]))  


#memformat decimal places   
for x in range(10):
	data_mmi_per_tipe[x] = float("{0:.3f}".format(data_mmi_per_tipe[x]))  

#memformat decimal places   
for x in range(10):
	loss_value_per_tipe[x] = float("{0:.3f}".format(loss_value_per_tipe[x]))  

#memformat decimal places   
for x in range(10):
	data_damage_per_tipe[x] = float("{0:.3f}".format(data_damage_per_tipe[x]))  

#inisiasi list data summary
data_summary = [[0,0,0,0,0,0,0,0] for x in range(10) ]

#update element of list dari data summary
for x in range(0,10):
	data_summary[x][0] = type_building[x]
	data_summary[x][1] = jumlah_tipe[x]
	data_summary[x][2] = data_mmi_per_tipe[x]
	data_summary[x][3] = loss_value_per_tipe[x]
	data_summary[x][4] = data_damage_per_tipe[x]
	data_summary[x][5] = rerata_mmi[x]
	data_summary[x][6] = rerata_loss[x]


#mengurutkan list berdasarkan nilai array
data_summary.sort(key=lambda x: int(x[3]))


#menampilkan data summary menggunakan PrettyTable
print("")
print("---------------------------------------------------DATA HASIL KOMPUTASI---------------------------------------------------")
print("")
t = PrettyTable(['No','Building Type','Number of Buildings','Loss Value per Type', 'Total of MMI','Average of Loss Value','Average of MMI'])
for x in range(10):
	t.add_row([x+1,data_summary[x][0], data_summary[x][1],data_summary[x][3],data_summary[x][2],data_summary[x][6],data_summary[x][5]])
print(t)


loss_graph = [item[3] for item in data_summary]
mmi_graph = [item[2] for item in data_summary]
type_building_graph = [item[0] for item in data_summary]
rerata_mmi_graph = [item[5] for item in data_summary]
rerata_loss_graph = [item[6] for item in data_summary]


fig, ax = plt.subplots()

#menyesuaikan posisi plot di gambar
plt.subplots_adjust(top=0.85)

#set plot line
ax.plot(loss_graph, mmi_graph,marker='o', label='Loss vs MMI for each Building Type')

#set annotation in line marker
for i, txt in enumerate(type_building_graph):
    ax.annotate(txt, (loss_graph[i],mmi_graph[i]))

#mengatur label X
plt.xlabel('Total of Loss Value')
#mengatur label Y
plt.ylabel('Total of MMI')
#menampilkan legend
plt.legend(loc=(0.305,1.1))  

#menampilkan gambar plot
plt.show()