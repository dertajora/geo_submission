#import library
import matplotlib.pyplot as plt
import numpy as np
import csv

#fungsi untuk membaca file csv
def read_csv(file_name):
	open_file = open(file_name)
	read_file = csv.reader(open_file)
	data = list(read_file)
	return data

#memanggil fungsi untuk membaca masing-masing csv
data_mmi = read_csv('mmi.csv')
data_building_value = read_csv('building_value.csv')
data_building_type = read_csv('building_type.csv')

#convert array 2D string into float
for x in range(20):
	for y in range(20):
		data_mmi[x][y] = float(data_mmi[x][y])
		data_building_type[x][y] = int(data_building_type[x][y])
		data_building_value[x][y] = int(data_building_value[x][y])

#menampilkan grafik MMI
fig, ax = plt.subplots(1)
p = ax.pcolormesh(data_mmi)
fig.colorbar(p)
ax.set_title("Grafik Plot 2D MMI")


#menampilkan grafik building value
fig, ax = plt.subplots(1)
p = ax.pcolormesh(data_building_value)
fig.colorbar(p)
ax.set_title("Grafik Plot 2D Building Value")

#menampilkan grafik building type
fig, ax = plt.subplots(1)
p = ax.pcolormesh(data_building_type)
fig.colorbar(p)
ax.set_title("Grafik Plot 2D Building Type")

print("Pembuatan Grafik Plot 2D Berhasil !")
#menampilkan gambar plot
plt.show()



